#!/bin/sh
#
# Install script for git.autistici.org/ai/website
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

# Packages that are only used to build the site. These will be
# removed once we're done.
BUILD_PACKAGES="rsync"

# Packages required to serve the website and run the services, in
# addition to those already installed by the base apache2 image.
PACKAGES="
  libapache2-mod-sso
  php-json
  php-imap
  php-intl
  php-mbstring
  php-xml
  php-zip
  mariadb-client
"

# Apache modules to enable.
APACHE_MODULES_ENABLE="
  rewrite
  sso
  unique_id
"

# Sites to enable.
APACHE_SITES="
  webmail
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
    }
fi

set -e
umask 022

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Install the configuration, overlayed over /etc.
chmod -R o-w /tmp/conf
rsync -a /tmp/conf/ /etc/

# Setup Apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2ensite ${APACHE_SITES}

# Ensure that the startup script is executable.
chmod 755 /start.sh

# Create config mountpoint
mkdir -p /etc/roundcube

# Link all plugin configuration files back to /etc/roundcube, so we
# can provide the entire configuration by bind-mounting a single
# directory.
ln -sf /etc/roundcube/config.inc.php /var/www/webmail/config/config.inc.php
for plugin in $(cd /var/www/webmail/plugins && echo *); do
    test -d /var/www/webmail/plugins/${plugin} || continue
    rm -f /var/www/webmail/plugins/${plugin}/config.inc.php || true
    ln -sf /etc/roundcube/${plugin}.config.inc.php \
       /var/www/webmail/plugins/${plugin}/config.inc.php
done

# Create writable storage mountpoint
mkdir -p /data

# Fix runtime permissions for the Roundcube data directories.
for d in temp logs ; do
    rm -fr /var/www/webmail/$d || true
    ln -sf /data/$d /var/www/webmail/$d
done

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf

