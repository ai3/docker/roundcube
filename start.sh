#!/bin/sh
#
# Start RoundCube.
#
# This script is invoked before starting apache2 and the PHP
# runner. It copies the configuration files in their desired location,
# and runs database upgrades if necessary.
#

CONFIG_DIR=/etc/roundcube
TARGET_DIR=/var/www/webmail

# Name of the MySQL database to use.
MYSQL_DB="ai_roundcube"

# If ${CONFIG_DIR}/my.cnf exists, use it as a defaults file for mysql.
MYSQL=mysql
if [ -e "${CONFIG_DIR}/my.cnf" ]; then
    MYSQL="${MYSQL} --defaults-file=${CONFIG_DIR}/my.cnf"
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

# Abort if some fundamental environment variables are not defined.
# This is nicer than having Apache die with an error later.
test -n "${DOMAIN}" \
    || die "the 'DOMAIN' environment variable is not defined"
test -n "${SHARD_ID}" \
    || die "the 'SHARD_ID' environment variable is not defined"

# Verify that the main Roundcube configuration is in place.
test -e ${CONFIG_DIR}/config.inc.php \
    || die "${CONFIG_DIR}/config.inc.php is missing"

# Now set up the MySQL database for Roundcube. The current Roundcube
# version is stored in the database.
rc_version=$(cat ${TARGET_DIR}/.rc_version)
cur_version=$(${MYSQL} -NBe 'select version from rc_version' ${MYSQL_DB} 2>/dev/null)

# If cur_version is empty, it means that the database has not been
# initialized yet.
if [ -z "${cur_version}" ]; then
    ${MYSQL} ${MYSQL_DB} < ${TARGET_DIR}/SQL/mysql.initial.sql \
        || die "could not load initial SQL schema to ${MYSQL_DB}"
    ${MYSQL} -e "create table rc_version (version text); insert into rc_version (version) values ('unknown')" ${MYSQL_DB} \
        || die "could not create rc_version table in ${MYSQL_DB}"
fi

# Run database upgrade if necessary.
if [ "${rc_version}" != "${cur_version}" ]; then
    ${TARGET_DIR}/bin/update.sh -v ${cur_version} -y \
        || die "Roundcube update script failed"

    ${MYSQL} -e "update rc_version set version='${rc_version}'" ${MYSQL_DB}
fi

exit 0
