<?php

// Relative root URL path for Content-Security-Policy script-src.
// Set it to the relative URL path of the Roundcube installation.
$config['csp_script_path'] = '/';

// If non-empty, the proto + hostname to allow SSO requests,
// e.g. https://sso.domain.org
$config['csp_sso_hostname'] = '';
