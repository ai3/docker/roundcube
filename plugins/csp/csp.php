<?php

/**
 * Content-Security-Policy
 *
 * Set Content-Security-Policy headers on all responses.
 *
 * @version 0.1.1
 * @author Autistici/Inventati
 */

class csp extends rcube_plugin {
  public function init() {
    $this->load_config();
    $this->add_hook('send_page', array($this, 'add_header'));
  }

  public function add_header($content) {
    $rcmail = rcmail::get_instance();
    $host = $_SERVER['HTTP_HOST'];
    $proto = rcube_utils::https_check() ? 'https' : 'http';
    $path = $rcmail->config->get('csp_script_path', '/');
    $src_sso = "";
    $sso_hostname = $rcmail->config->get('csp_sso_hostname', '');
    if($sso_hostname != '') {
      $src_sso .= "{$sso_hostname}";
    }
    $csp_header = (
        "default-src 'self' {$src_sso}; " .
        "img-src https://*; " .
        "script-src 'self' {$proto}://{$host}{$path} 'unsafe-inline' 'unsafe-eval' {$src_sso}; " .
        "style-src 'self' 'unsafe-inline' {$src_sso}; " .
        "object-src 'none'");
    $report_uri = $rcmail->config->get('csp_report_uri', '');
    if($report_uri != '') {
      $csp_header .= '; report-uri ' . $report_uri;
    }
    $report_to = $rcmail->config->get('csp_report_to', '');
    if($report_to != '') {
      $csp_header .= '; report-to ' . $report_to;
    }
    header("Content-Security-Policy: {$csp_header}");
    return $content;
  }
}

