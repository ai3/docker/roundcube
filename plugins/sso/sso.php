<?php

/**
 * Single-sign on with ai/sso and mod_sso.
 *
 * @version 0.1
 * @author ale
 * @website https://git.autistici.org/ai3/docker-roundcube
 * @licence GNU GPLv3+
 */

class sso extends rcube_plugin
{
  private $redirect_query;

  function init()
  {
    $this->add_hook('startup', array($this, 'startup'));
    $this->add_hook('authenticate', array($this, 'authenticate'));
    $this->add_hook('login_after', array($this, 'login'));
    $this->add_hook('login_failed', array($this, 'login_failed'));
    $this->add_hook('logout_after', array($this, 'logout'));
    $this->add_hook('storage_connect', array($this, 'storage_connect'));
  }

  function startup($args)
  {
    /*
      The purpose of this hook is to set $_SESSION['password'] to the
      SSO ticket, so that the rest of the Roundcube functionality can
      use it to login.
    */
    if (!empty($_SERVER['SSO_USER']) && !empty($_SERVER['SSO_TICKET'])) {
      if (empty($_SESSION['user_id'])) {
        $args['action'] = 'login';
        $this->redirect_query = $_SERVER['QUERY_STRING'];
      } else {
        $_SESSION['password'] = null;
      }
    }

    return $args;
  }

  function authenticate($args)
  {
    if (!empty($_SERVER['SSO_USER'])) {
      $args['user'] = $_SERVER['SSO_USER'];
      $args['pass'] = null;
    }

    $args['cookiecheck'] = false;
    $args['valid'] = true;

    return $args;
  }

  function logout($args) {
    // Redirect to global SSO logout path.
    $this->load_config();

    $sso_logout_url = rcmail::get_instance()->config->get('sso_logout_url');
    header("Location: " . $sso_logout_url, true, 307);
    exit;
  }

  function login($args)
  {
    if ($this->redirect_query) {
      header('Location: ./?' . $this->redirect_query);
      exit;
    }
    return $args;
  }

  function login_failed($args)
  {
    $this->load_config();
    $sso_logout_url = rcmail::get_instance()->config->get('sso_logout_url');
    echo('<h3>There has been a login error with the mail server. Please try <a href="' . $sso_logout_url . '">logging out</a> and logging in again.</h3>');
    exit;
  }

  function storage_connect($args)
  {
    if (!empty($_SERVER['SSO_USER']) && !empty($_SERVER['SSO_TICKET'])) {
      $args['user'] = $_SERVER['SSO_USER'];
      $args['pass'] = $_SERVER['SSO_TICKET'];
    }
    return $args;
  }

}
