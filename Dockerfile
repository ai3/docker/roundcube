FROM debian:stable AS build

ENV RC_VERSION=1.6.10
ENV SAUSERPREFS_VERSION=1.20.1

ADD . /build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends bash curl make patch rsync default-jre-headless wget unzip npm
RUN /build/install-roundcube --version=${RC_VERSION} --destdir=/build/src build && echo ${RC_VERSION} > /build/src/.rc_version

FROM registry.git.autistici.org/ai3/docker/apache2-php-base:bookworm

COPY conf /tmp/conf
COPY build.sh /tmp/build.sh
COPY start.sh /start.sh
COPY --from=build /build/src /var/www/webmail
RUN /tmp/build.sh && rm -fr /tmp/build.sh /tmp/conf

